provider "aws" {
  region = "eu-west-1"
}

resource "aws_key_pair" "admin" {
  key_name   = "admin"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCrvo6tpzXAPveL27T9ObnmZY0kALqrDO+T3DgxKcM/z+Tzd0pPMBeZ/3LX0g82u5Wm8MfP7KWmtS9+YKmEAyqAAgBR18vj5uz/pJYGne56sKeCPPdD/9lo3EqcOc6A+SA1spSMGBCiGDWYh8b/3/02Dv4jr33G0wmLbRhuCaRE4ZIxcDuAVRv2AR3DF4Vb+Ge1zbAetzRgsEDOrsW7HjQJucCmUJi7FTezPYFd/cusDJM/lihZrUm7lz0bo8P9vmGyHvNENFosUgwT6kfgSmrI+G7ZqvXi8xnGyWGVkqHCnFg2mKiQWE+EZpgmBiJN2IrLSHed7WjVjTxiUueKQ5Mf9KFLqnab7e60Q5U0m3Omv54DatW/PhFWJ1UoHflOQFHf/1Yy/7gPXQXPXLzdfSkOsUsaQcnuYZkHQiESJvnPhxHh/brB4icDF2lItvCchQ2CF+I/MAvaqziL0TKqXWlZTC0tbxg/IKW7ElqDePFC3oOF9rnxnuSjr8JAc33GO/s= kali@kali"
}

resource "aws_default_vpc" "vpc" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "web" {
  vpc_id = aws_default_vpc.vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my-ec2" {
  ami           = "ami-02df9ea15c1778c9c"
  instance_type = "ubuntu"
  key_name      = "admin"
  iam_instance_profile = "${aws_iam_instance_profile.test_profile.name}"
  depends_on = [aws_key_pair.admin]

}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "DevOps"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key         = "TestTableHashKey"

   attribute {
    name = "TestTableHashKey"
    type = "S"
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}

resource "aws_iam_role" "role" {
  name = "test-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "test_profile" {
  name  = "test_profile"
  role= "${aws_iam_role.role.name}"
}