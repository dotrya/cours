from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello World!"


@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"


@app.route("/save-name")
def save_name():
    name = request.args.get("name")
    if name is None:
        return "please give a name"

    dynamodb = boto3.resource("dynamodb")
    dynamodb.put_item(TableName='DevOps', Item={'name': {'S': name}})

    return f"{name} saved!"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
